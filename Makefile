PANDOC ?= pandoc
talk.pptx: talk.md template.pptx
	$(PANDOC) $< -o $@ --reference-doc=template.pptx
